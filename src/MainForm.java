import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MainForm extends Application {

    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        //切换默认的应用观感
        Application.setUserAgentStylesheet(Application.STYLESHEET_MODENA);
        //加载默认布局
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/index.fxml"));
        //主窗体设置
        primaryStage.setResizable(false);
        primaryStage.setAlwaysOnTop(true);
        primaryStage.setTitle("华容道求解工具");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }
}
