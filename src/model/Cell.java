package model;

/**
 * 单个格网
 */
public class Cell {
    /**
     * 此网格的编号
     */
    private char code;
    /**
     * 网格的列号
     */
    private int col;
    /**
     * 网格的行号
     */
    private int row;

    /**
     * 网格构造方法
     *
     * @param code 编号
     * @param row  行号
     * @param col  列号
     */
    public Cell(int row, int col,char code) {
        this.row = row;
        this.col = col;
        this.code = code;
    }

    /**
     * 使用字符串的散列值
     *
     * @return
     */
    @Override
    public int hashCode() {
        return this.toString().hashCode();
    }

    /**
     * 相等的判断依据是字符串是否相等
     *
     * @param o
     *
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cell anotherCell = (Cell) o;
        return this.toString().equals(anotherCell.toString());
    }

    /**
     * 网格的字符串表示
     *
     * @return
     */
    @Override
    public String toString() {
        return String.format("Cell:(%d,%d,%c)", this.getRow(), this.getCol(), this.getCode());
    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

    public char getCode() {
        return code;
    }

    public void setCol(int col) {
        this.col = col;
    }

    public void setRow(int row) {
        this.row = row;
    }
}
